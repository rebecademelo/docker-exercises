#!/bin/bash

docker run -d -p 80:80 --name nginxcontainer_1 -v volume:/usr/share/nginx/html nginx:latest
docker run -d --name nginxcontainer_2 -v volume:/usr/share/nginx/html nginx:latest

docker run --name container_3 -d -ti -v volume:/files image
docker exec container_3 /bin/bash -c "echo '<h1>Olá mundo!</h1>' >> /files/main.html"
